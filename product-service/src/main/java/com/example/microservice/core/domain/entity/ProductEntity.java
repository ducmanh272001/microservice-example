package com.example.microservice.core.domain.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

@Table(
        name= "product"
)
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEntity {
    @Id
    @GeneratedValue(generator = "product_id_sq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
}

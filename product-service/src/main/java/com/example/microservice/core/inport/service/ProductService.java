package com.example.microservice.core.inport.service;

import com.example.microservice.app.api.ProductApi;
import com.example.microservice.app.dto.CreateProductRequest;
import com.example.microservice.app.dto.GetProductResponse;
import com.example.microservice.app.dto.UpdateProductRequest;
import com.example.microservice.core.domain.entity.ProductEntity;
import com.example.microservice.core.outport.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductService implements ProductApi {
    @Autowired
    private ProductRepository repository;

//    private final Product

    @Override
    public GetProductResponse getProduct(Long productId) {
        ProductEntity entity = repository.findById(productId).get();
        GetProductResponse response = GetProductResponse
                .builder()
                .price(entity.getPrice())
                .name(entity.getName())
                .description(entity.getDescription())
                .id(entity.getId())
                .build();
        return response;
    }

    @Override
    public void createProduct(CreateProductRequest request) {
        ProductEntity productEntity = ProductEntity.builder()
                .name(request.getName())
                .description(request.getName())
                .price(request.getPrice())
                .build();
        repository.save(productEntity);
        log.info("Save Product Success Id {}", productEntity.getId());

    }

    @Override
    public void updateProduct(Long productId, UpdateProductRequest request) {

    }

    @Override
    public List<GetProductResponse> getProducts() {
        List<ProductEntity> productEntities = repository.findAll();;
        return productEntities.stream().map(
                entity -> this.getProduct(entity.getId())
        ).collect(Collectors.toList());
    }




    @Override
    public void deleteProduct(Long productId) {

    }
}

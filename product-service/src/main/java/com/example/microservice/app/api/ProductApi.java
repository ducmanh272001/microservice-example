package com.example.microservice.app.api;

import com.example.microservice.app.dto.CreateProductRequest;
import com.example.microservice.app.dto.GetProductResponse;
import com.example.microservice.app.dto.UpdateProductRequest;

import java.util.List;

public interface ProductApi {

    public GetProductResponse getProduct(Long productId);
    public void createProduct(CreateProductRequest request);
    public void updateProduct(Long productId, UpdateProductRequest request);
    public List<GetProductResponse> getProducts();

    void deleteProduct(Long productId);
}

package com.example.microservice.app.rest;


import com.example.microservice.app.api.ProductApi;
import com.example.microservice.app.dto.CreateProductRequest;
import com.example.microservice.app.dto.GetProductResponse;
import com.example.microservice.app.dto.UpdateProductRequest;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/system/product")
@RequiredArgsConstructor
public class ProductController implements ProductApi {
    private final ProductApi productService;
    @Override
    @Operation(summary = "get product")
    @GetMapping
    public GetProductResponse getProduct(@RequestParam(name = "productId") Long productId) {
        return productService.getProduct(productId);
    }

    @Override
    @PostMapping
    public void createProduct(@RequestBody CreateProductRequest request) {
        productService.createProduct(request);
    }

    @Override
    @PutMapping("/{product-id}")
    public void updateProduct(@PathVariable(name = "product-id") Long productId, @RequestBody UpdateProductRequest request) {
       productService.updateProduct(productId,request);
    }

    @Override
    @GetMapping("/list")
    public List<GetProductResponse> getProducts() {
        return productService.getProducts();
    }

    @Override
    @DeleteMapping("/{product-id}")
    public void deleteProduct(@PathVariable(name = "product-id")Long productId) {
        productService.deleteProduct(productId);
    }
}

package com.example.microservice.app.dto;


import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class GetProductResponse {
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
}

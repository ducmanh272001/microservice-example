package com.example.microservice.core.domain.entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "stock")
@Data
@Builder
public class StockEntity {

    @Id
    @GeneratedValue(generator = "stock_id_sq", strategy = GenerationType.SEQUENCE)
    private String id;
    private String sku;
    private Integer quantity;
}

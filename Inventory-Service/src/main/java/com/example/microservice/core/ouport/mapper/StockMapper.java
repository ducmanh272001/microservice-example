package com.example.microservice.core.ouport.mapper;

import com.example.microservice.app.dto.CreateStockRequest;
import com.example.microservice.app.dto.CreateStockResponse;
import com.example.microservice.app.dto.GetStockResponse;
import com.example.microservice.core.domain.entity.StockEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StockMapper {
    GetStockResponse mapToGetStockResponse(StockEntity entity);
    StockEntity mapToStockEntity(CreateStockRequest request);
    CreateStockResponse mapToCreateStockRequest(StockEntity entity);
}

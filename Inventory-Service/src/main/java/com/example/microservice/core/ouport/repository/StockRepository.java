package com.example.microservice.core.ouport.repository;

import com.example.microservice.core.domain.entity.StockEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockRepository extends MongoRepository<StockEntity, Long> {
    List<StockEntity> findAllBySkuIn(List<String> skuCodes);

    StockEntity findById(String id);
}

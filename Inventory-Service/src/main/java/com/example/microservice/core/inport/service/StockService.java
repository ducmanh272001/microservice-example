package com.example.microservice.core.inport.service;

import com.example.microservice.app.api.StockApi;
import com.example.microservice.app.dto.CreateStockRequest;
import com.example.microservice.app.dto.CreateStockResponse;
import com.example.microservice.app.dto.GetStockClientResponse;
import com.example.microservice.app.dto.GetStockResponse;
import com.example.microservice.core.domain.entity.StockEntity;
import com.example.microservice.core.ouport.mapper.StockMapper;
import com.example.microservice.core.ouport.repository.StockRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StockService implements StockApi {
    private final StockMapper mapper;
    private final StockRepository repository;

    @Override
    @Transactional
    public CreateStockResponse createStock(CreateStockRequest request) {
        StockEntity entity = mapper.mapToStockEntity(request);
        repository.save(entity);
        return mapper.mapToCreateStockRequest(entity);
    }

    @Override
    public GetStockResponse getStock(String stockId) {
        return mapper.mapToGetStockResponse(
                repository.findById(stockId)
        );
    }

    @Override
    public List<GetStockResponse> getStocks() {
        List<StockEntity> stockEntities = repository.findAll();
        return stockEntities.stream().map(
                mapper::mapToGetStockResponse
        ).collect(Collectors.toList());
    }

    @Override
    public List<GetStockClientResponse> getStockClients(List<String> skuCodes) throws InterruptedException {
        log.info("Đang chờ xử lý vui lòng chờ ....");
        Thread.sleep(10000);
        log.info("Đã xử lý xong");
        List<StockEntity> stockEntities = repository.findAllBySkuIn(skuCodes);
        List<GetStockClientResponse> getStockClientResponses = stockEntities
                .stream().map(stockEntity ->
                        GetStockClientResponse.builder()
                                .sku(stockEntity.getSku())
                                .isInStock(stockEntity.getQuantity() > 0)
                                .build()
                ).collect(Collectors.toList());
        return getStockClientResponses;
    }
}

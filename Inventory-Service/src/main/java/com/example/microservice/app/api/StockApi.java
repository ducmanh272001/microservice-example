package com.example.microservice.app.api;

import com.example.microservice.app.dto.CreateStockRequest;
import com.example.microservice.app.dto.CreateStockResponse;
import com.example.microservice.app.dto.GetStockClientResponse;
import com.example.microservice.app.dto.GetStockResponse;

import java.util.List;

public interface StockApi {
    CreateStockResponse createStock(CreateStockRequest request);
    GetStockResponse getStock(String stockId);
    List<GetStockResponse> getStocks();
    List<GetStockClientResponse> getStockClients(List<String> skuCodes) throws InterruptedException;
}

package com.example.microservice.app.dto;

import lombok.Data;

@Data
public class CreateStockResponse {
    private String id;
}

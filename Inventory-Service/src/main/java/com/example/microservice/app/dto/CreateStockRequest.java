package com.example.microservice.app.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreateStockRequest {
    private String sku;
    private Integer quantity;
}

package com.example.microservice.app.rest;

import com.example.microservice.app.api.StockApi;
import com.example.microservice.app.dto.CreateStockRequest;
import com.example.microservice.app.dto.CreateStockResponse;
import com.example.microservice.app.dto.GetStockClientResponse;
import com.example.microservice.app.dto.GetStockResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/stock")
@RequiredArgsConstructor
public class StockController implements StockApi {

    private final StockApi stockService;
    @Override
    @PostMapping
    public CreateStockResponse createStock(@RequestBody CreateStockRequest request) {
        return stockService.createStock(request);
    }

    @Override
    @GetMapping("/{stockId}")
    public GetStockResponse getStock(@PathVariable(name = "stockId",required = false) String stockId) {
        return stockService.getStock(stockId);
    }

    @Override
    @GetMapping("/list")
    public List<GetStockResponse> getStocks() {
        return stockService.getStocks();
    }

    @Override
    @GetMapping("/skuCodes")
    public List<GetStockClientResponse> getStockClients(@RequestParam(name = "skuCodes",required = false) List<String> skuCodes) throws InterruptedException {
        return stockService.getStockClients(skuCodes);
    }
}

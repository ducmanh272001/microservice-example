package com.example.microservice.core.inport.service;

import com.example.microservice.app.api.OrderLineApi;
import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.order.GetOrderResponse;
import com.example.microservice.app.dto.orderline.CreateOrderLinesRequest;
import com.example.microservice.app.dto.orderline.GetOrderLineResponse;
import com.example.microservice.app.dto.orderline.UpdateOrderLineRequest;
import com.example.microservice.app.dto.stock.GetStockClientResponse;
import com.example.microservice.core.domain.entity.OrderEntity;
import com.example.microservice.core.domain.entity.OrderLineEntity;
import com.example.microservice.core.inport.error.CommandException;
import com.example.microservice.core.inport.error.CommandExceptionBuilder;
import com.example.microservice.core.inport.errors.ErrorCodes;
import com.example.microservice.core.outport.mapper.OrderLineMapper;
import com.example.microservice.core.outport.mapper.OrderMapper;
import com.example.microservice.core.outport.repository.OrderLineRepository;
import com.example.microservice.core.outport.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderLineService implements OrderLineApi {
    private final OrderLineRepository repository;
    private final OrderLineMapper mapper;
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final StockService stockService;

    @Override
    public GetOrderLineResponse getOrderLine(Long orderLineId) {
        OrderLineEntity entity = repository.findById(orderLineId).orElseThrow(
                () -> CommandExceptionBuilder.exception(ErrorCodes.ORDER_LINE_IS_NOT_FOUND)
        );
        GetOrderLineResponse response = mapper.mapToGetOrderLineResponse(entity);

        response.setOrder(
                orderMapper.mapToGetOrderResponse(
                        orderRepository.findById(response.getId()).get()
                )
        );
        return response;
    }

    @Override
    public List<GetOrderLineResponse> getOrderLines() {
        List<OrderLineEntity> orderLineEntities = repository.findAll();

        //Find All List Order To Map
        List<Long> orderIds = orderLineEntities.stream().map(
                OrderLineEntity::getOrderId
        ).collect(Collectors.toList());

        List<OrderEntity> orderEntities = orderRepository.findByIdIn(orderIds);

        Map<Long, OrderEntity> mapOrder = orderEntities.stream().collect(Collectors.toMap(
                OrderEntity::getId, Function.identity()
        ));

        List<GetOrderLineResponse> orderLineResponses = orderLineEntities.stream().map(
                mapper::mapToGetOrderLineResponse
        ).collect(Collectors.toList());
        orderLineResponses.forEach(
                orderLine -> {
                    OrderEntity orderEntity = mapOrder.get(orderLine.getOrderId());
                    GetOrderResponse orderResponse = orderMapper.mapToGetOrderResponse(orderEntity);
                    orderLine.setOrder(orderResponse);
                }
        );
        return orderLineResponses;
    }

    @Override
    public void createOrderLine(CreateOrderLinesRequest request,OrderEntity orderEntity) {
        List<OrderLineEntity> orderLineEntities = request.getCreateData()
                .stream().map(mapper::mapToOrderLineEntity).collect(Collectors.toList());
        orderLineEntities.forEach(
                orderLineEntity -> orderLineEntity.setOrderId(orderEntity.getId())
        );
        //Khi find sku trong stock mà còn hàng bằng true thì sẽ cho bán nêú không thì thôi
        List<String> skuCodes = orderLineEntities.stream().map(OrderLineEntity::getSkuCode).collect(Collectors.toList());

        List<GetStockClientResponse> stockClientResponses = stockService.getStockClientResponse(skuCodes);


        if (stockClientResponses.isEmpty()){
            throw CommandExceptionBuilder.exception(ErrorCodes.ORDER_IS_NOT_FOUND);
        }
        boolean allProductIsInStock = stockClientResponses.stream().allMatch(
                stockEntity-> stockEntity.getIsInStock().equals(true)
        );
        if (allProductIsInStock) {
            repository.saveAll(orderLineEntities);
        } else {
           log.info("Create Failed ! Because Stock Not Found");
           throw CommandExceptionBuilder.exception(ErrorCodes.CREATE_ORDER_FAILED);
        }
    }

    @Override
    public void updateOrderLine(UpdateOrderLineRequest request, Long id) {
        OrderLineEntity orderLineEntity = repository.findById(id)
                .orElseThrow(
                        () -> CommandExceptionBuilder.exception(ErrorCodes.ORDER_IS_NOT_FOUND)
                );
        mapper.updateOrderLine(orderLineEntity, request);
        repository.save(orderLineEntity);
    }
}

package com.example.microservice.core.inport.errors;

import com.example.microservice.core.inport.error.ErrorCode;

public final class ErrorCodes {

    public static final ErrorCode NO_ALSO_RO = new ErrorCode(20,"error.noAlsoRow");

    public static final ErrorCode ORDER_IS_NOT_FOUND = new ErrorCode(10,"error.orderIsNotFound");
    public static final ErrorCode ORDER_LINE_IS_NOT_FOUND = new ErrorCode(11,"error.orderLineIsNotFound");
    public static final ErrorCode CREATE_ORDER_FAILED = new ErrorCode(12,"error.createOrderFailed");


}

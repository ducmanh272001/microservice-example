package com.example.microservice.core.outport.proxy.dto;

import lombok.Data;

@Data
public class CreateStockResponse {
    private String id;
}

package com.example.microservice.core.outport.proxy.dto;

import lombok.Data;

import java.util.List;

@Data
public class BaseResponse<T> {
    List<T> data;
}

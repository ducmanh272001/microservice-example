package com.example.microservice.core.inport.error;

import java.util.Arrays;

public class CommandException extends RuntimeException{
    private final transient ErrorCode[] errorCodes;
    private final transient Object[] args;
    private final transient int http;

    public CommandException(String message, ErrorCode[] errorCodes, Object[] args) {
        super(message);
        this.errorCodes = errorCodes;
        this.args = args;
        this.http = 200;
    }

    public CommandException(String message, ErrorCode[] errorCodes) {
        super(message);
        this.errorCodes = errorCodes;
        this.args = null;
        this.http = 200;
    }

    public CommandException(ErrorCode[] errorCodes) {
        this.errorCodes =errorCodes;
        this.http =200;
        this.args = null;
    }

    public CommandException(String message, ErrorCode[] errorCodes, int http) {
        super(message);
        this.errorCodes = errorCodes;
        this.http = http;
        this.args = null;
    }

    public CommandException(String message, ErrorCode[] errorCodes, Object[] args, int http) {
        super(message);
        this.errorCodes = errorCodes;
        this.args = args;
        this.http = http;
    }



    public int getHttp() {
        return http;
    }

    public String toString() {
        String var10000 = super.getMessage();
        return "CommandException[message=" + var10000 + ",errorCodes=" + Arrays.toString(this.errorCodes) + ",args=" + Arrays.toString(this.args) + "]";
    }

    public ErrorCode[] getErrorCodes() {
        return this.errorCodes;
    }

    public Object[] getArgs() {
        return this.args;
    }
}

package com.example.microservice.core.outport.mapper;

import com.example.microservice.app.dto.order.CreateOrderRequest;
import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.order.GetOrderResponse;
import com.example.microservice.app.dto.order.UpdateOrderRequest;
import com.example.microservice.core.domain.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    CreateOrderResponse mapToCreateOrderResponse(OrderEntity entity);
    GetOrderResponse mapToGetOrderResponse(OrderEntity entity);
    OrderEntity mapToOrderEntity(CreateOrderRequest request);
    void updateFromUpdateOrderRequest(@MappingTarget OrderEntity orderEntity, UpdateOrderRequest updateOrderRequest);
}

package com.example.microservice.core.outport.proxy.dto;

import lombok.Data;

@Data
public class GetStockResponse {
    private String id;
    private String sku;
    private Integer quantity;
}

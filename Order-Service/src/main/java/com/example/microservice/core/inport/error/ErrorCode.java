package com.example.microservice.core.inport.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.util.Arrays;

@Builder
public class ErrorCode {
    private int httpCode;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String messageCode;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String[] fields;

    public ErrorCode() {
    }

    public ErrorCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public ErrorCode(String messageCode) {
        this.httpCode = 200;
        this.messageCode = messageCode;
    }

    public ErrorCode(int httpCode, String messageCode) {
        this.httpCode = httpCode;
        this.messageCode = messageCode;
    }

    public ErrorCode(int httpCode, String messageCode, String[] fields) {
        this.httpCode = httpCode;
        this.messageCode = messageCode;
        this.fields = fields;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ErrorCode[code=").append(this.httpCode).append(",messageCode=").append(this.messageCode);
        if (this.fields != null) {
            builder.append(",fields=").append(Arrays.toString(this.fields));
        }

        builder.append("]");
        return builder.toString();
    }

    public int getHttpCode() {
        return this.httpCode;
    }

    public String getMessageCode() {
        return this.messageCode;
    }

    public String[] getFields() {
        return this.fields;
    }

    public void setHttpCode(final int httpCode) {
        this.httpCode = httpCode;
    }

    public void setMessageCode(final String messageCode) {
        this.messageCode = messageCode;
    }

    public void setFields(final String[] fields) {
        this.fields = fields;
    }
}

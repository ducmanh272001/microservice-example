package com.example.microservice.core.inport.service;

import com.example.microservice.app.dto.stock.GetStockClientResponse;
import com.example.microservice.core.outport.proxy.dto.GetStockResponse;
import com.example.microservice.infract.proxy.feign.StockServiceClient;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StockService {

    private final StockServiceClient client;

    @Transactional(readOnly = true)
    public List<GetStockResponse> getStocks() {
        return client.getStocks();
    }

    @Transactional(readOnly = true)
    public GetStockResponse getStock(String stockId) {
        var stockOk =client.getStock(stockId);
        return stockOk;
    }

    @Transactional(readOnly = true)
    public List<GetStockClientResponse> getStockClientResponse(List<String> skuCodes){
       var stockClient = client.getStockClients(skuCodes);
       return stockClient;
    }
}

package com.example.microservice.core.outport.proxy.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreateStockRequest {
    private String sku;
    private Integer quantity;
}

package com.example.microservice.core.inport.service;

import com.example.microservice.app.api.OrderApi;
import com.example.microservice.app.dto.order.CreateOrderRequest;
import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.order.GetOrderResponse;
import com.example.microservice.app.dto.order.UpdateOrderRequest;
import com.example.microservice.app.dto.orderline.GetOrderLineResponse;
import com.example.microservice.core.domain.entity.OrderEntity;
import com.example.microservice.core.domain.entity.OrderLineEntity;
import com.example.microservice.core.inport.error.CommandExceptionBuilder;
import com.example.microservice.core.inport.errors.ErrorCodes;
import com.example.microservice.core.outport.mapper.OrderLineMapper;
import com.example.microservice.core.outport.mapper.OrderMapper;
import com.example.microservice.core.outport.repository.OrderLineRepository;
import com.example.microservice.core.outport.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService implements OrderApi {
    private final OrderRepository repository;
    private final OrderMapper mapper;
    private final OrderLineRepository orderLineRepository;
    private final OrderLineMapper orderLineMapper;
    private final OrderLineService orderLineService;

    @Override
    public GetOrderResponse getOrder(Long orderId) {
        OrderEntity entity = repository.findById(orderId)
                .orElseThrow(
                        () -> CommandExceptionBuilder.exception(
                                ErrorCodes.ORDER_IS_NOT_FOUND
                        )
                );

        List<OrderLineEntity> orderLineEntities = orderLineRepository.findAll();
        List<GetOrderLineResponse> orderLineResponses = orderLineEntities.stream().map(
                orderLineMapper::mapToGetOrderLineResponse
        ).collect(Collectors.toList());

        Map<Long, List<GetOrderLineResponse>> mapAllGetOrderLineResponse = orderLineResponses.stream()
                .collect(Collectors.groupingBy(GetOrderLineResponse::getOrderId));
        GetOrderResponse response = mapper.mapToGetOrderResponse(entity);
        List<GetOrderLineResponse> responses = mapAllGetOrderLineResponse.get(response.getId());
        response.setOrderLine(responses);
        return response;
    }

    @Override
    public List<GetOrderResponse> getOrders() {
        List<OrderLineEntity> orderLineEntities = orderLineRepository.findAll();
        List<GetOrderLineResponse> orderLineResponses = orderLineEntities
                .stream().map(orderLineMapper::mapToGetOrderLineResponse).collect(Collectors.toList());
        Map<Long, List<GetOrderLineResponse>> mapOrderLine = orderLineResponses.stream()
                .collect(Collectors.groupingBy(GetOrderLineResponse::getOrderId));

        List<OrderEntity> orderEntities = repository.findAll();
        List<GetOrderResponse> orderResponses = new ArrayList<>();
        orderEntities.forEach(
                orderEntity -> {
                    List<GetOrderLineResponse> orderLineResponses1 = mapOrderLine.get(orderEntity.getId());
                    GetOrderResponse response = mapper.mapToGetOrderResponse(orderEntity);
                    response.setOrderLine(orderLineResponses1);
                    orderResponses.add(response);
                }
        );
        return orderResponses;
    }

    @Override
    public void update(Long orderId, UpdateOrderRequest updateOrderRequest) {
        OrderEntity orderEntity = repository.findById(orderId).orElseThrow(
                () -> CommandExceptionBuilder.exception(ErrorCodes.ORDER_IS_NOT_FOUND)
        );
        mapper.updateFromUpdateOrderRequest(orderEntity, updateOrderRequest);
        repository.save(orderEntity);
    }

    @Override
    public CreateOrderResponse create(CreateOrderRequest request) {
        OrderEntity orderEntity = mapper.mapToOrderEntity(request);
        repository.save(orderEntity);
        orderLineService.createOrderLine(request.getOrderLines(), orderEntity);
        return mapper.mapToCreateOrderResponse(orderEntity);
    }

    @Override
    public CompletableFuture<CreateOrderResponse> createOk(CreateOrderRequest request) {
        return null;
    }
}

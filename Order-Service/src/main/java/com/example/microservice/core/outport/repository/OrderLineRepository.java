package com.example.microservice.core.outport.repository;

import com.example.microservice.core.domain.entity.OrderLineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderLineRepository extends JpaRepository<OrderLineEntity,Long> {
}

package com.example.microservice.core.outport.mapper;

import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.orderline.CreateOrderLineRequest;
import com.example.microservice.app.dto.orderline.CreateOrderLineResponse;
import com.example.microservice.app.dto.orderline.GetOrderLineResponse;
import com.example.microservice.app.dto.orderline.UpdateOrderLineRequest;
import com.example.microservice.core.domain.entity.OrderEntity;
import com.example.microservice.core.domain.entity.OrderLineEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface OrderLineMapper {
    CreateOrderResponse mapToCreateOrderResponse(OrderLineEntity entity);
    OrderLineEntity mapToOrderLineEntity(CreateOrderLineRequest request);
    GetOrderLineResponse mapToGetOrderLineResponse(OrderLineEntity orderLineEntity);
    OrderLineEntity mapFromCreateOrderLineRequest(CreateOrderLineRequest request);
    void updateOrderLine(@MappingTarget OrderLineEntity orderLineEntity, UpdateOrderLineRequest update);
}

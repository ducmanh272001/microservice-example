package com.example.microservice.core.inport.error;

public class CommandExceptionBuilder {

    public CommandExceptionBuilder() {
    }

    public static CommandException exception(ErrorCode errorCode,  Object... args) {
        ErrorCode[] errorCodes = new ErrorCode[]{errorCode};
        return new CommandException("error.default", errorCodes, args);
    }

    public static CommandException exception(ErrorCode errorCode) {
        ErrorCode[] errorCodes = new ErrorCode[]{errorCode};
        return new CommandException("error.default", errorCodes);
    }

    public static CommandException exception(ErrorCode[] errorCodes,int http) {
        return new CommandException("error.default", errorCodes, http);
    }

    public static CommandException exception(String msgCode, ErrorCode errorCode, int http,Object... args) {
        ErrorCode[] errorCodes = new ErrorCode[]{errorCode};
        return new CommandException(msgCode, errorCodes, args, http);
    }

    public static CommandException exception(String msgCode, ErrorCode errorCode, int http) {
        ErrorCode[] errorCodes = new ErrorCode[]{errorCode};
        return new CommandException(msgCode, errorCodes, http);
    }

    public static CommandException exception(String msgCode, ErrorCode[] errorCodes, int http) {
        return new CommandException(msgCode, errorCodes, http);
    }

}

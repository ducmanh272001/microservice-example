package com.example.microservice.core.domain.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
@Table(name = "order_line")
public class OrderLineEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //Sku code
    private String skuCode;
    private BigDecimal price;
    private Integer quantity;
    private Long orderId;
}

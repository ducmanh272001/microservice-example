package com.example.microservice.core.inport.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@ControllerAdvice
@Slf4j
@Component
public class ExceptionHandlerAdvice {

    @Autowired
    private MessageSource messageSource;

//      private  final Tracer tracer;
//
//    public ExceptionHandlerAdvice(Tracer tracer) {
//        this.tracer = tracer;
//    }

    @ExceptionHandler(CommandException.class)
    public ResponseEntity<ResponseBody> handleException(CommandException e) {
        // log exception
        ErrorCode errorCode = (ErrorCode) Array.get(e.getErrorCodes(), 0);

        String messageName = messageSource.getMessage(errorCode.getMessageCode(), new Object[]{}, Locale.ENGLISH);
        String messageDefault = messageSource.getMessage(e.getMessage(), new Object[]{}, Locale.ENGLISH);

        ErrorCode errorBuilder = ErrorCode.builder()
                .messageCode(messageName)
                .httpCode(e.getHttp()).build();
//        //Convest From Array To List
        ErrorCode[] errorCodes = new ErrorCode[]{errorBuilder};
        List<ErrorCode> errorCodeList = Arrays.asList(errorCodes);


        ResponseBody responseBody = new ResponseBody();
        responseBody.setMessage(messageDefault);


//        Span span = tracer.currentSpan();

        responseBody.setTraceId("hello");

        responseBody.setErrorCodes(errorCodeList);
        log.info("Chuoi la {}", responseBody);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseBody);
    }
}

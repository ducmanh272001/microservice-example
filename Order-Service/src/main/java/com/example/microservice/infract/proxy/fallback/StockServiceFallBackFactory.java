package com.example.microservice.infract.proxy.fallback;

import com.example.microservice.app.dto.stock.GetStockClientResponse;
import com.example.microservice.core.outport.proxy.dto.BaseResponse;
import com.example.microservice.core.outport.proxy.dto.GetStockResponse;
import com.example.microservice.infract.proxy.feign.StockServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;

import java.util.List;

@Slf4j
public class StockServiceFallBackFactory implements FallbackFactory<StockServiceClient> {

    @Override
    public StockServiceClient create(Throwable cause) {
        log.error("something error from stock service client !");
        return new StockServiceClient() {

            @Override
            public List<GetStockResponse> getStocks() {
                log.info("log error get stocks !");
                return null;
            }

            @Override
            public GetStockResponse getStock(String stockId) {
                log.info("log error get stocks !");
                return null;
            }

            @Override
            public List<GetStockClientResponse> getStockClients(List<String> skuCodes) {
                log.info("log error get stocks !");
                return null;
            }
        };
    }
}


package com.example.microservice.infract.proxy.feign;

import com.example.microservice.app.dto.stock.GetStockClientResponse;
import com.example.microservice.core.outport.proxy.dto.GetStockResponse;
import com.example.microservice.infract.proxy.fallback.StockServiceFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "INVENTORY-SERVICE",fallback = StockServiceFallBackFactory.class)
//@LoadBalancerClient(name = "INVENTORY-SERVICE",configuration = LoadBalanceConfiguration.class)
public interface StockServiceClient {

    @GetMapping(value = "/api/v1/stock/list")
    List<GetStockResponse> getStocks();

    @GetMapping(value = "/api/v1/stock/{stockId}")
    GetStockResponse getStock(@PathVariable(name = "stockId",required = false)String stockId);

    @GetMapping(value = "/api/v1/stock/skuCodes")
    List<GetStockClientResponse> getStockClients(@RequestParam(name = "skuCodes",required = false) List<String> skuCodes);
}

package com.example.microservice.app.api;

import com.example.microservice.app.dto.order.CreateOrderRequest;
import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.order.GetOrderResponse;
import com.example.microservice.app.dto.order.UpdateOrderRequest;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface OrderApi {
    GetOrderResponse getOrder(Long orderId);
    List<GetOrderResponse> getOrders();
    void update(Long orderId, UpdateOrderRequest updateOrderRequest);
    CreateOrderResponse create(CreateOrderRequest request);
    CompletableFuture<CreateOrderResponse> createOk(CreateOrderRequest request);
}

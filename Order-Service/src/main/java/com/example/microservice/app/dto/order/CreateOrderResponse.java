package com.example.microservice.app.dto.order;

import lombok.Data;

@Data
public class CreateOrderResponse {
    private Long id;
}

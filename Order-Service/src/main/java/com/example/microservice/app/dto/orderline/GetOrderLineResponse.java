package com.example.microservice.app.dto.orderline;

import com.example.microservice.app.dto.order.GetOrderResponse;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GetOrderLineResponse {
    private Long id;
    private String skuCode;
    private BigDecimal price;
    private Integer quantity;
    private Long orderId;
    private GetOrderResponse order;
}

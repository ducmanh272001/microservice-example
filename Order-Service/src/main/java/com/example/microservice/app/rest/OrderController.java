package com.example.microservice.app.rest;

import com.example.microservice.app.api.OrderApi;
import com.example.microservice.app.dto.order.CreateOrderRequest;
import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.order.GetOrderResponse;
import com.example.microservice.app.dto.order.UpdateOrderRequest;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
@Slf4j
public class OrderController implements OrderApi {
    private final OrderApi orderService;

    @Override
    @GetMapping("/{orderId}")
    public GetOrderResponse getOrder(@PathVariable(value = "orderId", required = false) Long orderId) {
        return orderService.getOrder(orderId);
    }

    @Override
    @GetMapping("/list")
    public List<GetOrderResponse> getOrders() {
        return orderService.getOrders();
    }

    @Override
    @PutMapping("/{orderId}")
    public void update(@PathVariable(value = "orderId", required = false) Long orderId,
                       @RequestBody UpdateOrderRequest updateOrderRequest) {
        orderService.update(orderId, updateOrderRequest);
    }

    @Override
    public CreateOrderResponse create(@RequestBody CreateOrderRequest request) {
        // TODO document why this method is empty
        return null;
    }

    @Override
    @PostMapping
    @CircuitBreaker(name = "inventory-manh", fallbackMethod = "fallBackMethod")
    @TimeLimiter(name = "inventory-manh")
    @Retry(name = "inventory-manh")
    public CompletableFuture<CreateOrderResponse> createOk(@RequestBody CreateOrderRequest request) {
        return CompletableFuture.supplyAsync(() -> orderService.create(request));
    }

    public CompletableFuture<CreateOrderResponse> fallBackMethod(CreateOrderRequest request, RuntimeException runtimeException) {
        log.info("Quá thời gian chờ xin vui lòng thử lại sau");
        return null;
    }


}

package com.example.microservice.app.dto.order;

import lombok.Data;

@Data
public class UpdateOrderRequest {
    private String orderNumber;
}

package com.example.microservice.app.dto.order;

import com.example.microservice.app.dto.orderline.CreateOrderLinesRequest;
import lombok.Data;

@Data
public class CreateOrderRequest {

    private String orderNumber;
    private CreateOrderLinesRequest orderLines;
}

package com.example.microservice.app.dto.order;

import com.example.microservice.app.dto.orderline.GetOrderLineResponse;
import lombok.Data;

import java.util.List;

@Data
public class GetOrderResponse {
    private Long id;
    private String orderNumber;
    private List<GetOrderLineResponse> orderLine;
}

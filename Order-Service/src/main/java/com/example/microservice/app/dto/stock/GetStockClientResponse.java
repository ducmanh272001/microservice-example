package com.example.microservice.app.dto.stock;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetStockClientResponse {
    //Xem như là sku code;
    private String sku;
    private Boolean isInStock;
}

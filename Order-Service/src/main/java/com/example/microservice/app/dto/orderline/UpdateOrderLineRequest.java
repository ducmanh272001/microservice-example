package com.example.microservice.app.dto.orderline;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UpdateOrderLineRequest {
    private String skuCode;
    private BigDecimal price;
    private Integer quantity;
    private Long orderId;
}

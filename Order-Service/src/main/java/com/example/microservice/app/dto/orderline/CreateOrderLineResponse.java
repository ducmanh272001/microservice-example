package com.example.microservice.app.dto.orderline;

import lombok.Data;

@Data
public class CreateOrderLineResponse {
    private Long id;
}

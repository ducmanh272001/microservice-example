package com.example.microservice.app.dto.orderline;

import lombok.Data;

import java.util.List;

@Data
public class CreateOrderLinesRequest {
    List<CreateOrderLineRequest> createData;
}

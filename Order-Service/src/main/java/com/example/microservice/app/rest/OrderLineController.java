package com.example.microservice.app.rest;

import com.example.microservice.app.api.OrderLineApi;
import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.orderline.CreateOrderLineRequest;
import com.example.microservice.app.dto.orderline.CreateOrderLinesRequest;
import com.example.microservice.app.dto.orderline.GetOrderLineResponse;
import com.example.microservice.app.dto.orderline.UpdateOrderLineRequest;
import com.example.microservice.core.domain.entity.OrderEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/order-line")
@RequiredArgsConstructor
public class OrderLineController implements OrderLineApi {

    private final OrderLineApi orderLineService;

    @Override
    @GetMapping("/{orderLineId}")
    public GetOrderLineResponse getOrderLine(@PathVariable(value = "orderLineId",required = false) Long orderLineId) {
        return orderLineService.getOrderLine(orderLineId);
    }

    @Override
    @GetMapping("/list")
    public List<GetOrderLineResponse> getOrderLines() {
        return orderLineService.getOrderLines();
    }

    @Override
    @PostMapping
    public void createOrderLine(@RequestBody CreateOrderLinesRequest request, @RequestBody OrderEntity orderEntity) {
       orderLineService.createOrderLine(request,orderEntity);
    }

    @Override
    @PutMapping("/{orderLineId}")
    public void updateOrderLine(@RequestBody UpdateOrderLineRequest request,
                                @PathVariable(value = "orderLineId",required = false) Long id) {
       orderLineService.updateOrderLine(request,id);
    }
}

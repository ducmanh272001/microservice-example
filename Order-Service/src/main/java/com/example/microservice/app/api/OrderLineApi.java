package com.example.microservice.app.api;

import com.example.microservice.app.dto.order.CreateOrderResponse;
import com.example.microservice.app.dto.orderline.CreateOrderLineRequest;
import com.example.microservice.app.dto.orderline.CreateOrderLinesRequest;
import com.example.microservice.app.dto.orderline.GetOrderLineResponse;
import com.example.microservice.app.dto.orderline.UpdateOrderLineRequest;
import com.example.microservice.core.domain.entity.OrderEntity;

import java.util.List;

public interface OrderLineApi {
    GetOrderLineResponse getOrderLine(Long orderLineId);
    List<GetOrderLineResponse>getOrderLines();
    void createOrderLine(CreateOrderLinesRequest request, OrderEntity orderEntity);
    void updateOrderLine(UpdateOrderLineRequest request, Long id);
}

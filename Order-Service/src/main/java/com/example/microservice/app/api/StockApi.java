package com.example.microservice.app.api;

import com.example.microservice.app.dto.stock.GetStockClientResponse;
import com.example.microservice.core.outport.proxy.dto.GetStockResponse;

import java.util.List;

public interface StockApi {
    List<GetStockResponse> getStocks();

    GetStockResponse getStock(String stockId);

    List<GetStockClientResponse> getStockClientResponses(List<String> skuCodes);
}

package com.example.microservice.app.rest;

import com.example.microservice.app.api.StockApi;
import com.example.microservice.app.dto.stock.GetStockClientResponse;
import com.example.microservice.core.inport.service.StockService;
import com.example.microservice.core.outport.proxy.dto.BaseResponse;
import com.example.microservice.core.outport.proxy.dto.GetStockResponse;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/stock")
@RequiredArgsConstructor
public class StockController implements StockApi {

    private final StockService service;
    @Override
    @GetMapping("/list")
    public List<GetStockResponse> getStocks() {
        var response =service.getStocks();
        return response;
    }

    @Override
    @GetMapping
    public GetStockResponse getStock(@RequestParam(name = "stockId", required = false) String stockId) {
        return service.getStock(stockId);
    }

    @Override
    @GetMapping("/skuCode")
    public List<GetStockClientResponse> getStockClientResponses(@RequestParam(name = "skuCodes",required = false) List<String> skuCodes) {
        return service.getStockClientResponse(skuCodes);
    }
}
